# -*- coding: utf-8 -*-

import time
import subprocess

from termcolor import colored, cprint
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class Watcher:
    DIRECTORY_TO_WATCH = "./ihungry/static/components/"

    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        cprint("=== Watching components ===", "red", "on_cyan")
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            cprint("=== Error ===", "red", "on_white")

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            cprint("=== Received created event - {}. ===".format(event.src_path), "red", "on_cyan")

        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            cprint("=== Component modified - {}. ===".format(event.src_path), "red", "on_cyan")
            subprocess.call("make build_react", shell=True)
            cprint("=== Components built ===", "red", "on_cyan")

if __name__ == '__main__':
    w = Watcher()
    w.run()
