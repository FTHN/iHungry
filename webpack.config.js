//require our dependencies

var path = require('path')
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')


function handle_configs(configs) {
    let main_config = new Array();
    for (config of configs) {
      main_config.push(
        {
          context: __dirname,
          entry: `./ihungry/static/components/${config}`,
          output: {
              path: path.resolve(`./ihungry/static/components/bundles/${config}`),
              filename: '[name]-[hash].js',
          },
          plugins: [
            new BundleTracker({
                filename: `./webpack-stats/${config}.json`
            }),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery'
            }),
            new webpack.DefinePlugin({
              'process.env': {
                NODE_ENV: JSON.stringify('production')
              }
            }),
            new webpack.optimize.UglifyJsPlugin()
          ],
          module: {
            rules: [
              {
                test: /\.css$/,
                use: [
                  { loader: "style-loader" },
                  { loader: "css-loader" },
                ],
              },
              {
                test: /\.useable\.css$/,
                use: [
                  {
                    loader: "style-loader",
                    options: {
                      useable: true
                    },
                  },
                  { loader: "css-loader" },
                ],
              },
              {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015'],
                    plugins: ["transform-class-properties"]
                }
              }
            ]
          },
          resolve: {
            modules: ["node_modules", path.resolve(__dirname, "app")],
            extensions: [".js", ".json", ".jsx", ".css"],
          }
        }
      )
    }
    return main_config
}

module.exports = handle_configs(
  [
    'dashboard', 'report_child_feeding',
    'report_product', 'report_delivery', 'report_route'
  ]
)
