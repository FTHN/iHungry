# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views


urlpatterns = [
    url(r'', admin.site.urls),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^_nested_admin/', include('nested_admin.urls')),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^fthn/', include('ihungry.fthn.urls', 'fthn')),
    url(r'^api/v1/', include('ihungry.api.urls', namespace='api_v1')),
    url(r'^fthn/dashboard/', include('ihungry.dashboard.urls', 'jet-dashboard')),
    url(r'^reports/', include('ihungry.reports.urls', namespace='reports')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'rosetta' in settings.INSTALLED_APPS:
        urlpatterns += [
            url(r'^rosetta/', include('rosetta.urls')),
        ]

    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ]
