'use-strict';

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import Cookies from 'universal-cookie';
import Select from 'react-select';
import ReactTable from 'react-table';
import {PieChart, Pie, Tooltip, Cell, ResponsiveContainer} from 'recharts';

import FeedingPoint from './reports/feeding';

import 'react-table/react-table.css';
import 'react-select/dist/react-select.css';
import './styles/report.css';


class Report extends Component {

  state = {
    feedingOptions: [],
    feedingValues: [],
    reportData: []
  }

  print() {
    window.print();
  }

  constructor(props) {
    super(props)
    this.baseURL = `/api/v1/report-child-feeding/`;
  }

  componentDidMount() {
    this.getFeedings();
  }

  getFeedings = () => {
    return fetch(this.baseURL)
      .then((response) => {
        return response.json();
      }).then((data) => {
        this.setState({feedingOptions: data});
      });
  }

  feedingSelectChange = (feeding) => {
    this.setState(function(prevState, props){
      return {feedingValues: feeding}
    });
  }

  handleFilter = () => {
    if (this.state.feedingValues.length > 0) {
      if(!$('#wrapper-filter').hasClass('rcf')) {
        $('.wrapper-filter').removeClass('wrapper-filter').addClass('reports-filter rcf');
        $('.wrapper-content').removeClass('hidden');
      }
      $('.Select-control').removeClass('filter-error');
      let url = `${this.baseURL}?feedings=${this.state.feedingValues.map(feeding => feeding.value)}`
      return fetch(url)
        .then((response) => {
          return response.json();
        }).then((data) => {
          this.setState({reportData: data});
        });
    } else {
      $('.Select-control').addClass('filter-error');
    }
  }

  render() {
    const cookies = new Cookies();
    const lang = cookies.get('djlang');
    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

    return (
      <div>
        <div className='wrapper-filter' id='wrapper-filter'>
          <h1>{lang === 'es' ? `Reporte de Niños` : `Childrens Report`}</h1>
          <Select multi
            onChange={this.feedingSelectChange}
            value={this.state.feedingValues}
            options={this.state.feedingOptions}
            name="form-field-fp"
            placeholder={lang === 'es' ? `Seleccione Puntos de Alimentacion(s)` : `Choose Feeding Point(s)`} />
          <button className='button' onClick={this.handleFilter}>{lang === 'es' ? `Filtrar Información` : `Filter Information`}</button>
        </div>
        <div className='wrapper-content hidden'>
          <h1>{lang === 'es' ? `Reporte de Niños` : `Childrens Report`}</h1>
          {this.state.reportData ?
            this.state.reportData.map((row, index) => {
              return (
                <div className="reportSummary" key={index}>
                  <div className="dashboard-container columns_2 cf">
                    <FeedingPoint feeding={row.feeding} />
                    <div className="dashboard-column-wrapper">
                      <div className="dashboard-item">
                        <div className="dashboard-item-header">
                          <span className="dashboard-item-header-title">{lang === 'es' ? `Cantidad de niños activos e inactvos` : `Number of childrens active and inactive`}</span>
                        </div>
                        <div className="dashboard-item-content">
                          <ResponsiveContainer width="100%" height={250}>
                            <PieChart>
                              <Pie data={row.chart}
                                cx="50%" cy="50%"
                                outerRadius={80}
                                fill="#8884d8" label>
                                {row.chart.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)}
                              </Pie>
                              <Tooltip/>
                            </PieChart>
                          </ResponsiveContainer>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="dashboard-item-content">
                    <table className="table">
                      <thead>
                        <tr>
                          <th>{lang === 'es' ? `Punto de Alimentacion`: `Feeding Point`}</th>
                          <th>{lang === 'es' ? `Nombre`: `First Name`}</th>
                          <th>{lang === 'es' ? `Apellido`: `Last Name`}</th>
                          <th>{lang === 'es' ? `Edad`: `Age`}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {row.records.map((row, index) => {
                          return (
                            <tr key={index}>
                              <td>{row.feeding}</td>
                              <td>{row.firstName}</td>
                              <td>{row.lastName}</td>
                              <td>{row.age}</td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              );
            })
          : null}
          <button onClick={this.print} className="button print-button">{lang === 'es' ? `Imprimir` : `Print`}</button>
        </div>
      </div>
    )
  }
}

ReactDOM.render(
  <Report/>, document.getElementById('container-report')
)
