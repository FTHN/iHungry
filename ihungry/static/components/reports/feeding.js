'use-strict';

import React, { Component } from 'react'

export default class FeedingPoint extends Component {
  render() {

    const feeding = this.props.feeding;
    const lang = this.props.lang;

    return (
      <div className="dashboard-column-wrapper">
        <div className="dashboard-item">
          <div className="dashboard-item-header">
            <span className="dashboard-item-header-title">{lang === 'es' ? `Datos del Punto de Alimentacion` : `Feeding Point Data`}</span>
          </div>
          <div className="dashboard-item-content">
            <table className="table">
              <tbody>
                <tr>
                  <td>{lang === 'es' ? `Punto De Alimentacion:` : `Feeding Point:`}</td>
                  <td>{feeding.name}</td>
                </tr>
                <tr>
                  <td>{lang === 'es' ? `Direccion:` : `Address:`}</td>
                  <td>{feeding.dir}</td>
                </tr>
                <tr>
                  <td>{lang === 'es' ? `Departamento:` : `Department:`}</td>
                  <td>{feeding.dept}</td>
                </tr>
                <tr>
                  <td>{lang === 'es' ? `Municipio:` : `Municipality`}</td>
                  <td>{feeding.mun}</td>
                </tr>
                <tr>
                  <td>{lang === 'es' ? `Niños registrados:` : `Children registered`}</td>
                  <td>{feeding.childs}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}
