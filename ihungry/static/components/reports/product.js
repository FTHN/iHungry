'use-strict';

import React, { Component } from 'react'

export default class ProductDetail extends Component {

  render() {
    const product = this.props.data.product;
    const sti = this.props.data.sti;
    const sto = this.props.data.sto;
    const lang = this.props.lang;

    return (
      <div>
        <table style={{margin: '0 auto'}}>
          <tbody>
            <tr>
              <td>{lang === 'es' ? `Nombre Producto` : `Product Name`}: {product.name}</td>
              <td>{lang === 'es' ? `Unidades Disponibles` : `Available Units`}: {product.units}</td>
              <td>{lang === 'es' ? `Unidad de Medida` : `Unit of measure`}: {product.um}</td>
              <td>{lang === 'es' ? `Peso` : `Weight`}: {product.kg}</td>
            </tr>
          </tbody>
        </table>
        <div className="dashboard-container columns_2 cf">
          <div className="dashboard-column-wrapper">
            <div className="dashboard-item">
              <div className="dashboard-item-header">
                <span className="dashboard-item-header-title">{lang === 'es' ? `Transacciones Entrantes` : `Incoming transactions`}</span>
              </div>
              <div className="dashboard-item-content">
                <table className="table">
                  <thead>
                    <tr>
                      <td>{lang === 'es' ? `Cantidad`: `Quantity`}</td>
                      <td>{lang === 'es' ? `Fecha`: `Date`}</td>
                      <td>{lang === 'es' ? `Tipo` : `Type`}</td>
                    </tr>
                  </thead>
                  <tbody>
                  {sti.map((row, index) => {
                    return (
                      <tr key={index}>
                        <td>{row.qty}</td>
                        <td>{row.date}</td>
                        <td>{row.type}</td>
                      </tr>
                    )
                  })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="dashboard-column-wrapper">
            <div className="dashboard-item">
              <div className="dashboard-item-header">
                <span className="dashboard-item-header-title">{lang === 'es' ? `Transacciones Salientes` : `Outbound transactions`}</span>
              </div>
              <div className="dashboard-item-content">
                <table className="table">
                  <thead>
                    <tr>
                      <td>{lang === 'es' ? `Cantidad` : `Quantity`}</td>
                      <td>{lang === 'es' ? `Fecha` : `Date`}</td>
                      <td>{lang === 'es' ? `Tipo` : `Type`}</td>
                    </tr>
                  </thead>
                  <tbody>
                  {sto.map((row, index) => {
                    return (
                      <tr key={index}>
                        <td>{row.qty}</td>
                        <td>{row.date}</td>
                        <td>{row.type}</td>
                      </tr>
                    )
                  })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
