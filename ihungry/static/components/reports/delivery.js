'use-strict';

import React, { Component } from 'react'

import _chunk from 'lodash/chunk';


export default class Delivery extends Component {

  productList = (products) => {
    return products.map((product, index) => {
      return (
        <tr key={index}>
          <td>{product.name}</td>
          <td>{product.qty} {product.mu}</td>
        </tr>
      )
    })
  }

  deliveryRow = (deliveries) => {
    const lang = this.props.lang
    return deliveries.map((delivery, index) => {
      return (
        <div className="dashboard-column-wrapper" key={index}>
          <div className="dashboard-item">
            <div className="dashboard-item-header">
              <span className="dashboard-item-header-title">{lang === 'es' ? `Entrega` : `Delivery`}</span>
            </div>
            <div className="dashboard-item-content">
              <table className="table">
                <tbody>
                  <tr>
                    <td className="bold">{lang === 'es' ? `Fecha` : `Date`}</td>
                    <td className="bold">{lang === 'es' ? `Estado` : `State`}</td>
                  </tr>
                  <tr>
                    <td>{delivery.date}</td>
                    <td>{delivery.status}</td>
                  </tr>
                  <tr>
                    <td colSpan={2} className="text-center bold">{lang === 'es' ? `Productos` : `Products`}</td>
                  </tr>
                  <tr>
                    <td className="bold">{lang === 'es' ? `Nombre` : `Name`}</td>
                    <td className="bold">{lang === 'es' ? `Cantidad` : `Quantity`}</td>
                  </tr>
                  {delivery.products.length ? (
                    this.productList(delivery.products)
                  ) : (
                    <tr>
                      <td colSpan={2}>{lang === 'es' ? `No Productos` : `No Products`}</td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      )
    })
  }

  render() {
    let deliveries = _chunk(this.props.deliveries, 3);

    return (
      <div>
        {deliveries.map((delivery_row, index) => {

          let container_class = `dashboard-container columns_${delivery_row.length} cf`;

          return (
            <div className={container_class} key={index}>
              {this.deliveryRow(delivery_row)}
            </div>
          )
        })}
      </div>
    )

  }
}
