'use-strict';

import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Cookies from 'universal-cookie';

import FeedingDepartment from './dashboard/feeding_department';
import ChildrenDepartment from './dashboard/children_department';
import FoodHistory from './dashboard/food_history';
import TableDashboard from './dashboard/table_dashboard';
import StockHistory from './dashboard/stock_history';

import './styles/report.css';


class Dashboard extends Component {
  render() {
    const cookies = new Cookies();
    const lang = cookies.get('djlang');
    let langs = []

    if ( lang === 'es' ) {
      langs.push(
        `Numero de Puntos de Alimentacion por Departamento`,
        `Cantidad de Niños por departamento`,
        `Historico de transacciones - Ultimo Mes`,
        `Ultimas transacciones`,
        `Ultimos puntos de Alimentacion`,
        `Ultimos Niños`,
        `Historico de entregas completadas - Ultimos seis meses`
      )
    } else {
      langs.push(
        `Number of Feeding Points by Department`,
        `Number of children per Department`,
        `Transaction history - Last Month`,
        `Latest transactions`,
        `Latest Feeding Points`,
        `Latest Childrens`,
        `History of completed deliveries - Last six months`
      )
    }

    return (
      <div>
        <div className="dashboard-container columns_2 cf">
          <FeedingDepartment title={langs[0]} legend={lang === 'es' ? `Puntos de Alimentación` : `Feeding Points`} />
          <ChildrenDepartment title={langs[1]} legend={lang === 'es' ? `Niños` : `Childrens`} />
        </div>
        <div className="dashboard-container columns_1 cf">
          <FoodHistory title={langs[2]} />
        </div>
        <div className="dashboard-container columns_3 cf">
          <TableDashboard
            title={langs[3]}
            endpoint='/api/v1/transaction/' />
          <TableDashboard
            title={langs[4]}
            endpoint='api/v1/feeding-point/' />
          <TableDashboard
            title={langs[5]}
            endpoint='api/v1/children2/' />
        </div>
        <div className="dashboard-container columns_1 cf">
          <StockHistory title={langs[6]} />
        </div>
      </div>
    )
  }
}

ReactDOM.render(
  <Dashboard />,
  document.getElementById('container-dashboard')
)
