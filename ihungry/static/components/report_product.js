'use-strict';

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import Cookies from 'universal-cookie';
import Select from 'react-select';
import { DateRangePicker } from 'react-dates';

import ProductDetail from './reports/product'

import 'react-select/dist/react-select.css';
import 'react-dates/lib/css/_datepicker.css';
import './styles/report.css';


class ReportProduct extends Component {
  state = {
    startDate: null,
    endDate: null,
    focusedInput: null,
    prodOptions: [],
    prodValues: [],
    prodData: [],
  }

  formatDate(date) {
    if (date) {
      return date.toISOString().slice(0, 10);
    }
  }

  print() {
    window.print();
  }

  componentDidMount() {
    this.getProducts();
  }

  calendarInfoPanel = () => {
    return (
      <div
        style={{
          padding: '10px 21px',
          borderTop: '1px solid #dce0e0',
          color: '#484848',
        }}
      ></div>
    )
  }

  getProducts = () => {
    return fetch(`/api/v1/report-product`)
      .then((response) => {
        return response.json();
      }).then((data) => {
        this.setState({prodOptions: data});
      });
  }

  prodSelectChange = (prd) => {
    this.setState(function(prevState, props){
      return {prodValues: prd}
    });
  }

  handleFilter = () => {
    if (this.state.startDate && this.state.endDate && this.state.prodValues.length > 0) {
      if(!$('#wrapper-filter').hasClass('rcf')) {
        $('.wrapper-filter').removeClass('wrapper-filter').addClass('reports-filter rcf');
        $('.wrapper-content').removeClass('hidden');
      }
      $('.DateRangePickerInput').removeClass('filter-error');
      $('.Select-control').removeClass('filter-error');
      let startDate = `?startDate=${this.formatDate(this.state.startDate)}`
      let endDate = `&endDate=${this.formatDate(this.state.endDate)}`
      let products = `&products=${this.state.prodValues.map(prod => prod.value)}`
      return fetch(`/api/v1/report-product/${startDate}${endDate}${products}`)
        .then((response) => {
          return response.json();
        }).then((data) => {
          this.setState({prodData: data});
        });
    } else {
      $('.DateRangePickerInput').addClass('filter-error');
      $('.Select-control').addClass('filter-error');
    }
  }

  render() {
    const cookies = new Cookies();
    const lang = cookies.get('djlang');

    return (
      <div>
        <div className='wrapper-filter' id='wrapper-filter'>
          <h1>{lang === 'es' ? `Reporte de Producto` : `Product Report`}</h1>
          <DateRangePicker
            startDate={this.state.startDate}
            startDatePlaceholderText={lang === 'es' ? `Fecha Inicio` : `Start Date`}
            endDate={this.state.endDate}
            endDatePlaceholderText={lang === 'es' ? `Fecha Fin`: `End Date`}
            withFullScreenPortal={true}
            showClearDates={true}
            showDefaultInputIcon={true}
            numberOfMonths={3}
            isOutsideRange={() => false}
            onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })}
            focusedInput={this.state.focusedInput}
            onFocusChange={focusedInput => this.setState({ focusedInput })}
            renderCalendarInfo={this.calendarInfoPanel}
          />
          <Select multi
            onChange={this.prodSelectChange}
            options={this.state.prodOptions}
            value={this.state.prodValues}
            name="form-field-prod"
            placeholder={lang === 'es' ? `Seleccione Producto(s)` : `Choose Product(s)`} />
          <button className='button' onClick={this.handleFilter}>{lang === 'es' ? `Filtrar Información` : `Filter Information`}</button>
        </div>
        <div className='wrapper-content hidden'>
          <h1>{lang === 'es' ? `Reporte de Producto` : `Product Report`}</h1>
          <h2>{`${this.formatDate(this.state.startDate)} - ${this.formatDate(this.state.endDate)}`}</h2>
          {this.state.prodData ?
            this.state.prodData.map((row, index) => {
              return (
                <div className="reportSummary" key={index}>
                  <ProductDetail data={row} lang={lang} />
                </div>
              );
            })
          : null}
          <button onClick={this.print} className="button print-button">{lang === 'es' ? `Imprimir` : `Print`}</button>
        </div>
      </div>
    )
  }
}

ReactDOM.render(
  <ReportProduct />, document.getElementById('container-report')
)
