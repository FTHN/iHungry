'use-strict';

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import Cookies from 'universal-cookie';
import Select from 'react-select';
import { DateRangePicker } from 'react-dates';
import {PieChart, Pie, Tooltip, Cell, ResponsiveContainer} from 'recharts';

import FeedingPoint from './reports/feeding';
import Delivery from './reports/delivery';

import 'react-select/dist/react-select.css';
import 'react-dates/lib/css/_datepicker.css';
import './styles/report.css';


class ReportDelivery extends Component {
  state = {
    startDate: null,
    endDate: null,
    focusedInput: null,

    feedingOptions: [],
    feedingValues: [],

    stateOptions: [],
    stateValues: [],

    reportData: [],
  }

  formatDate(date) {
    if (date) {
      return date.toISOString().slice(0, 10);
    }
  }

  print() {
    window.print();
  }

  componentDidMount() {
    this.getFeedings();
    this.bumpStatusSelect();
  }

  bumpStatusSelect() {
    let baseStatus = [
      {"label": "Pendiente", "value": 1},
      {"label": "Confirmado", "value": 2},
      {"label": "Diferido", "value": 3},
      {"label": "Entregado", "value": 4}
    ];
    this.setState({
      stateOptions: baseStatus
    });
  }

  calendarInfoPanel = () => {
    return (
      <div
        style={{
          padding: '10px 21px',
          borderTop: '1px solid #dce0e0',
          color: '#484848',
        }}
      ></div>
    )
  }

  getFeedings = () => {
    return fetch(`/api/v1/report-delivery`)
      .then((response) => {
        return response.json();
      }).then((data) => {
        this.setState({feedingOptions: data});
      });
  }

  feedingSelectChange = (feeding) => {
    this.setState(function(prevState, props){
      return {feedingValues: feeding}
    });
  }

  stateSelectChange = (state) => {
    this.setState(function(prevState, props){
      return {stateValues: state}
    });
  }

  getReportData = () => {
    let startDate = `?startDate=${this.formatDate(this.state.startDate)}`
    let endDate = `&endDate=${this.formatDate(this.state.endDate)}`
    let feeding = `&feeding=${this.state.feedingValues.map(feed => feed.value)}`
    let state = `&state=${this.state.stateValues.map(state => state.value)}`
    return fetch(`/api/v1/report-delivery${startDate}${endDate}${feeding}${state}`)
      .then((response) => {
        return response.json();
      }).then((data) => {
        this.setState({reportData: data});
      });
  }

  handleFilter = () => {
    if (this.state.startDate && this.state.endDate &&
      this.state.feedingValues.length > 0 && this.state.stateValues.length > 0) {
      if(!$('#wrapper-filter').hasClass('rcf')) {
        $('.wrapper-filter').removeClass('wrapper-filter').addClass('reports-filter rcf report-delivery');
        $('.wrapper-content').removeClass('hidden');
      }
      $('.DateRangePickerInput').removeClass('filter-error');
      $('.Select-control').removeClass('filter-error');
      this.getReportData();
    } else {
      $('.DateRangePickerInput').addClass('filter-error');
      $('.Select-control').addClass('filter-error');
    }
  }

  render() {
    const cookies = new Cookies();
    const lang = cookies.get('djlang');
    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

    return (
      <div>
        <div className='wrapper-filter' id='wrapper-filter'>
          <h1>{lang === 'es' ? `Reporte de Entregas` : `Delivery Report`}</h1>
          <DateRangePicker
            startDate={this.state.startDate}
            startDatePlaceholderText={lang === 'es' ? `Fecha Inicio` : `Start Date`}
            endDate={this.state.endDate}
            endDatePlaceholderText={lang === 'es' ? `Fecha Fin`: `End Date`}
            withFullScreenPortal={true}
            showClearDates={true}
            showDefaultInputIcon={true}
            numberOfMonths={3}
            isOutsideRange={() => false}
            onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })}
            focusedInput={this.state.focusedInput}
            onFocusChange={focusedInput => this.setState({ focusedInput })}
            renderCalendarInfo={this.calendarInfoPanel}
          />
          <Select multi
            onChange={this.feedingSelectChange}
            options={this.state.feedingOptions}
            value={this.state.feedingValues}
            name="form-field-prod"
            placeholder={lang === 'es' ? `Seleccione Puntos de Alimentacion(s)` : `Select Feeding Point(s)`} />
          <Select multi
            onChange={this.stateSelectChange}
            options={this.state.stateOptions}
            value={this.state.stateValues}
            name="form-field-prod"
            placeholder={lang === 'es' ? `Seleccione Estado(s)` : `Choose State(s)`} />
          <button className='button' onClick={this.handleFilter}>{lang === 'es' ? `Filtrar Información` : `Filter Information`}</button>
        </div>
        <div className='wrapper-content hidden'>
          <h1>{lang === 'es' ? `Reporte de Entregas` : `Delivery Report`}</h1>
          <h2>{`${this.formatDate(this.state.startDate)} - ${this.formatDate(this.state.endDate)}`}</h2>
          {this.state.reportData ?
            this.state.reportData.map((row, index) => {
              return (
                <div className="reportSummary" key={index}>
                  <div className="dashboard-container columns_2 cf">
                    <FeedingPoint feeding={row.feeding} lang={lang} />
                    <div className="dashboard-column-wrapper">
                      <div className="dashboard-item">
                        <div className="dashboard-item-header">
                          <span className="dashboard-item-header-title">{lang === 'es' ? `Cantidad de niños activos e inactvos`: `Number of childrens active and inactive`}</span>
                        </div>
                        <div className="dashboard-item-content">
                          <ResponsiveContainer width="100%" height={250}>
                            <PieChart>
                              <Pie data={row.feeding.chart} cx="50%" cy="50%" outerRadius={80} fill="#8884d8" label>
                                {row.feeding.chart.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)}
                              </Pie>
                              <Tooltip/>
                            </PieChart>
                          </ResponsiveContainer>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Delivery deliveries={row.deliveries} lang={lang}/>
                </div>
              );
            })
          : null}
          <button onClick={this.print} className="button print-button">{lang === 'es' ? `Imprimir` : `Print`}</button>
        </div>
      </div>
    )
  }
}

ReactDOM.render(
  <ReportDelivery />, document.getElementById('container-report')
)
