'use-strict';

import React, { Component } from 'react'

export default class TableDashboard extends Component {

  state = {
    headers: [],
    records: []
  }

  componentDidMount() {
    let _self = this;
    $.ajax({
      url: this.props.endpoint,
      type: 'GET',
      dataType: 'json'
    })
    .done(function (resp)  {
      _self.setState({
        headers: resp.headers,
        records: resp.records
      });
    });
  }

  render() {
    function mapObject(object, callback) {
      return Object.keys(object).map(function (key) {
        return callback(key, object[key]);
      });
    }

    return (
      <div className="dashboard-column-wrapper">
        <div className="dashboard-item">
          <div className="dashboard-item-header">
            <span className="dashboard-item-header-title">{this.props.title}</span>
          </div>
          <div className="dashboard-item-content">
            <table className="table" style={{margin: '0 auto'}}>
              <thead>
                <tr>
                  {this.state.headers.map(function(elem, index) {
                    return <th>{elem}</th>
                  })}
                </tr>
              </thead>
              <tbody>
                {this.state.records.map(function(row, index) {
                  return (
                    <tr key={index}>
                    {mapObject(row, function (key, value) {
                      return <td>{value}</td>;
                    })}
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
