'use-strict';

import React, { Component } from 'react';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';


const CustomTooltip = React.createClass({

  render() {
    const { active } = this.props;

    if (active) {
      const { payload, label } = this.props;
      return (
        <div className="fthn-tooltip">
          <p className="label">{`${label} : ${payload[0].value}`}</p>
        </div>
      );
    }

    return null;
  }
})


export default class ChildrenDepartment extends Component {

  state = {
    data: []
  }

  componentDidMount() {
    let _self = this;
    $.ajax({
      url: '/api/v1/children/',
      type: 'GET',
      dataType: 'json'
    })
    .done(function (resp)  {
      const objs = [];
      for (let obj of resp) {
        objs.push({
          name: obj.name,
          nfp: obj.number_childs
        });
      }
      _self.setState({
        data: objs
      });
    });
  }

  render() {
    return (
      <div className="dashboard-column-wrapper">
        <div className="dashboard-item">
          <div className="dashboard-item-header">
            <span className="dashboard-item-header-title">{this.props.title}</span>
          </div>
          <div className="dashboard-item-content">
            <ResponsiveContainer>
              <BarChart data={this.state.data}>
                <XAxis dataKey="name"/>
                <YAxis />
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip content={<CustomTooltip/>} />
                <Legend
                  verticalAlign="top" height={36}
                  payload={[
                		{id: 'nfp', value: this.props.legend, type: 'circle', color: '#8884d8'}
                  ]} />
                <Bar dataKey="nfp" fill="#8884d8" />
              </BarChart>
            </ResponsiveContainer>
          </div>
        </div>
      </div>
    )
  }
}
