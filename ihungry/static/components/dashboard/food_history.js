'use-strict';

import React, { Component } from 'react';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';


export default class FoodHistory extends Component {
  state = {
    data: []
  }

  componentDidMount() {
    let _self = this;
    return fetch(`/api/v1/product-stock/`)
      .then((response) => {
        return response.json();
      }).then((data) => {
        _self.setState({data: data});
      });
  }

  render() {
    return (
      <div className="dashboard-column-wrapper">
        <div className="dashboard-item">
          <div className="dashboard-item-header">
            <span className="dashboard-item-header-title">{this.props.title}</span>
          </div>
          <div className="dashboard-item-content">
            <ResponsiveContainer>
              <BarChart data={this.state.data} margin={{top: 5, right: 30, left: 20, bottom: 5}}>
               <XAxis dataKey="name"/>
               <YAxis/>
               <CartesianGrid strokeDasharray="3 3"/>
               <Tooltip />
               <Legend verticalAlign="top" height={36} />
               <Bar dataKey="inp" fill="#8884d8" />
               <Bar dataKey="out" fill="#82ca9d" />
              </BarChart>
            </ResponsiveContainer>
          </div>
        </div>
      </div>
    )
  }
}
