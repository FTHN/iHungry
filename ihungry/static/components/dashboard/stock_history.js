'use-strict';

import React, { Component } from 'react';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';

export default class StockHistory extends Component {
  state = {
    data: []
  }

  componentDidMount() {
    let _self = this;
    return fetch(`/api/v1/stock-history/`)
      .then((response) => {
        return response.json();
      }).then((data) => {
        _self.setState({data: data});
      });
  }

  render() {
    return (
      <div className="dashboard-column-wrapper">
        <div className="dashboard-item">
          <div className="dashboard-item-header">
            <span className="dashboard-item-header-title">{this.props.title}</span>
          </div>
          <div className="dashboard-item-content">
            <ResponsiveContainer>
              <LineChart data={this.state.data} margin={{top: 5, right: 30, left: 20, bottom: 5}}>
               <XAxis dataKey="month" />
               <YAxis />
               <CartesianGrid strokeDasharray="3 3"/>
               <Tooltip />
               <Legend verticalAlign="top" height={36} />
               <Line type="monotone" dataKey="qty" stroke="#8884d8" activeDot={{r: 8}} />
              </LineChart>
            </ResponsiveContainer>
          </div>
        </div>
      </div>
    )
  }
}
