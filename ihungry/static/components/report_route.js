'use-strict';

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import Cookies from 'universal-cookie';
import Select from 'react-select';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';

import 'react-select/dist/react-select.css';
import './styles/report.css';


const CustomTooltip = React.createClass({

  render() {
    const { active } = this.props;

    if (active) {
      const { payload, label } = this.props;
      return (
        <div className="fthn-tooltip">
          <p className="label">{`${label} : ${payload[0].value} Entregas`}</p>
        </div>
      );
    }

    return null;
  }
})


class ReportRoute extends Component {
  state = {
    routeOptions: [],
    routeValues: [],
    reportData: [],
  }

  print() {
    window.print();
  }

  componentDidMount() {
    this.getRoutes();
  }

  getRoutes = () => {
    return fetch(`/api/v1/report-route`)
      .then((response) => {
        return response.json();
      }).then((data) => {
        this.setState({routeOptions: data});
      });
  }

  routeSelectChange = (route) => {
    this.setState(function(prevState, props){
      return {routeValues: route}
     });
  }

  handleFilter = () => {
    if (this.state.routeValues.length > 0) {
      if(!$('#wrapper-filter').hasClass('rcf')) {
        $('.wrapper-filter').removeClass('wrapper-filter').addClass('reports-filter rcf');
        $('.wrapper-content').removeClass('hidden');
      }
      $('.Select-control').removeClass('filter-error');
      let url = `/api/v1/report-route?routes=${this.state.routeValues.map(route => route.value)}`
      return fetch(url)
        .then((response) => {
          return response.json();
        }).then((data) => {
          this.setState({reportData: data});
        });
    } else {
      $('.Select-control').addClass('filter-error');
    }
  }

  render() {
    const cookies = new Cookies();
    const lang = cookies.get('djlang');
    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

    return (
      <div>
        <div className='wrapper-filter' id='wrapper-filter'>
          <h1>{lang === 'es' ? `Reporte de Ruta` : `Route Report`}</h1>
          <Select multi
            onChange={this.routeSelectChange}
            options={this.state.routeOptions}
            value={this.state.routeValues}
            name="form-field-prod"
            placeholder={lang === 'es' ? `Seleccione Ruta(s)` : `Choose Route(s)`} />
          <button className='button' onClick={this.handleFilter}>{lang === 'es' ? `Filtrar Información` : `Filter Information`}</button>
        </div>
        <div className='wrapper-content hidden'>
          <h1>{lang === 'es' ? `Reporte de Ruta` : `Route Report`}</h1>
          {this.state.reportData ?
            this.state.reportData.map((row, index) => {
              return (
                <div className="reportSummary" key={index}>
                  <div className="dashboard-container columns_2 cf">
                    <div className="dashboard-column-wrapper">
                      <div className="dashboard-item">
                        <div className="dashboard-item-content">
                          <table className="table">
                            <tbody>
                              <tr>
                                <td>{lang === 'es' ? `Nombre` : `Name`}:</td>
                                <td>{row.name}</td>
                              </tr>
                              <tr>
                                <td>{lang === 'es' ? `Conductor` : `Driver`}:</td>
                                <td>{row.driver}</td>
                              </tr>
                              <tr>
                                <td>{lang === 'es' ? `Vehiculo` : `Vehicle`}:</td>
                                <td>{row.vehicle}</td>
                              </tr>
                              <tr>
                                <td>{lang === 'es' ? `Fecha` : `Date`}:</td>
                                <td>{row.date}</td>
                              </tr>
                              <tr>
                                <td>{lang === 'es' ? `Total de Entregas` : `Total deliveries`}:</td>
                                <td>{row.delivery_total}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div className="dashboard-column-wrapper">
                      <div className="dashboard-item">
                        <div className="dashboard-item-content">
                          <ResponsiveContainer width="100%" height={250}>
                            <BarChart data={row.chart}>
                              <XAxis dataKey="name"/>
                              <YAxis />
                              <CartesianGrid strokeDasharray="3 3" />
                              <Tooltip content={<CustomTooltip/>} />
                              <Legend
                                verticalAlign="top" height={36}
                                payload={[
                              		{id: 'value', value: lang === 'es' ? 'Puntos de Alimentación': 'Feeding Points', type: 'circle', color: '#8884d8'}
                                ]} />
                              <Bar dataKey="value" fill="#8884d8" />
                            </BarChart>
                          </ResponsiveContainer>
                        </div>
                      </div>
                    </div>
                  </div>
                  {row.deliveries.map((delivery, index) => {
                    let column_class = `dashboard-container columns_${delivery.products.length} cf`
                    return (
                      <div className={column_class} key={index}>
                        <div className="dashboard-item">
                          <table style={{width: '100%'}}>
                            <tbody>
                              <tr>
                                <td className="bold">{lang === 'es' ? `Punto de Alimentación` : `Feeding Point`}</td>
                                <td>{delivery.feeding}</td>
                                <td className="bold">{lang === 'es' ? `Fecha` : `Date`}</td>
                                <td>{delivery.date}</td>
                                <td className="bold">{lang === 'es' ? `Estado` : `State`}</td>
                                <td>{delivery.status}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        {delivery.products.map((product, index) => {
                          return (
                            <div className="dashboard-column-wrapper" key={index}>
                              <div className="dashboard-item">
                                <div className="dashboard-item-content">
                                  <table>
                                    <thead>
                                      <tr>
                                        <th className="text-center bold">{lang === 'es' ? `Producto` : `Product`}</th>
                                        <th className="text-center bold">{lang === 'es' ? `Cantidad` : `Quantity`}</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td className="text-center">{product.name}</td>
                                        <td className="text-center">{product.qty}</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    )
                  })}
                  <div className="dashboard-container columns_1 cf">
                    <div className="dashboard-column-wrapper">
                      <div className="dashboard-item">
                        <div className="dashboard-item-content">
                          <p style={{padding: '10px'}}><strong>{lang === 'es' ? `Observaciones` : `Observations`}:</strong> {row.notes}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })
          : null}
          <button onClick={this.print} className="button print-button">{lang === 'es' ? `Imprimir` : `Print`}</button>
        </div>
      </div>
    )
  }
}

ReactDOM.render(
  <ReportRoute />, document.getElementById('container-report')
)
