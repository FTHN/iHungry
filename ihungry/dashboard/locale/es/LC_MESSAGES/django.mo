��    P      �  k         �  6   �                     *     =     J     X     l     s     |     �     �     �  "   �     �     �     �     �           &      F     g     t     z     �     �  
   �     �  	   �  	   �     �     �     �     �     �     �     �  g   	  e   l	  M   �	  K    
     l
     x
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  "   �
          '     F     d  -   �  !   �     �     �  	   �     �     �          )     0     5     ;     D     M     R     h          �     �     �  k  �  C        K     d     k     {     �     �     �     �     �  
   �     �     �       &        ?     U     i     x  &   �  '   �  $   �     �            	         *     ?     K     `     o     |     �     �     �     �     �     �  ~   �  |   e  f   �  c   I     �     �     �     �     �                     )     1     9     =  *   A  )   l  $   �  %   �  "   �  *     *   /     Z     s  	   y     �     �     �     �     �     �     �     �     �     �          /     8  
   ?     J     F      !   J   +       I   ;      (   2   	   8          @      "       %   =      '                  :   B       .   /       K   G   7   E       ?   L   &           M   O          A                       3       H      9   *                 1   )   $                 N   P           D                C   0              -   >      6          
          4          ,      5      <          #        Try to <a href="%s">revoke and grant access</a> again API request failed. Access Administration Application models Applications Bad arguments Bad server response By day By month By week Change Change password Counter Django "django-users" mailing list Django documentation Django irc channel External link Feed URL Google Analytics period visitors Google Analytics visitors chart Google Analytics visitors totals Grant access Group Inline Items Items limit Last month Last quarter Last week Last year Latest Django News Layout Link Links Log out Models Module not found Please <a href="%s">attach Google account and choose Google Analytics counter</a> to start using widget Please <a href="%s">attach Yandex account and choose Yandex Metrika counter</a> to start using widget Please <a href="%s">select Google Analytics counter</a> to start using widget Please <a href="%s">select Yandex Metrika counter</a> to start using widget Quick links RSS Feed Recent Actions Return to site Revoke access Show Stacked Statistics period Support Title Today URL Widget has been successfully added Widget was successfully updated Yandex Metrika period visitors Yandex Metrika visitors chart Yandex Metrika visitors totals You must install the FeedParser python module You must provide a valid feed URL application name children collapsed column counters loading failed grant access first module none order sessions settings user user dashboard module user dashboard modules users views visitors visits Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-05-11 09:41-0600
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
  Intente <a href="%s">revocar y garantizar el acceso</a> nuevamente Falló la petición API. Acceso Administración Modelos de la aplicación Aplicaciones Argumentos erróneos Respuesta negativa del servidor Por día Por mes Por semana Cambiar Cambiar contraseña Contador Lista de correos Django "django-users" Documentación Django Canal IRC de Django Enlace externo URL de Alimentador Periodo de visitantes Google Analytics Gráfico de visitantes Google Analytics Total de visitantes Google Analytics Garantizar acceso Grupo En linea Elementos Límite de elementos Último mes Último cuatrimestre Última semana Último año Últimas Noticias de Django Disposición Enlace Enlaces Cerrar sesión Modelos No se encontró el módulo Por favor <a href="%s">agregue una cuenta Google y elija un contador Google Analytics</a> para comenzar a usar este artilugio. Por favor <a href="%s">agregue una cuenta Yandex y elija un contador Yandex Metrika </a> para comenzar a usar este artilugio Por favor <a href="%s">seleccione un contador Google Analytics</a> para comenzar a usar este artilugio Por favor <a href="%s">seleccion un contador Yandex Metrika</a> para comenzar a usar este artilugio Enlaces Rápidos Alimentador RSS Acciones recientes Volver al sitio Revocar acceso Mostrar Apilado Periodo de estadísticas Soporte Título Hoy URL El artilugio ha sido exitosamente añadido El artilugio fue actualizado exitosamente Periodo de visitantes Yandex Metrika Gráfica de visitantes Yandex Metrika Total de visitantes Yandex Metrika Debe instalar el módulo python FeedParser Debe proveer un URL de alimentador válido nombre de la aplicación hijos colapsada columna falló la carga de contadores garantizar primer acceso módulo ninguno orden sesiones opciones usuario módulo de usuario en tablero módulos de usuario en tablero usuarios vistas visitantes visitas 