# -*- coding: utf-8 -*-
import datetime

from django.db import models
from django.db.models import Sum
from django.db.models.signals import post_save
from django.core.exceptions import ValidationError
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from ihungry.feeding_point.models import FeedingPoint, FeedingResponsable
from ihungry.food.models import Product, StockTransaction

from smart_selects.db_fields import ChainedForeignKey


class Vehicle(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    capacity = models.IntegerField(_('Tons Capacity'))

    class Meta:
        verbose_name = _('Vehicle')
        verbose_name_plural = _('Vehicles')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return "%s" % self.__unicode__()


class Driver(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    phone = models.CharField(_('Phone'), max_length=20)

    class Meta:
        verbose_name = _('Driver')
        verbose_name_plural = _('Drivers')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return "%s" % self.__unicode__()


class Delivery(models.Model):
    PENDING, CONFIRMED, DEFERRED, DELIVERED = range(1, 5)
    STATUS_CHOICES = (
        (PENDING, _('Pending')),
        (CONFIRMED, _('Confirmed')),
        (DEFERRED, _('Deferred')),
        (DELIVERED, _('Delivered'))
    )

    feeding = models.ForeignKey(FeedingPoint, verbose_name=_('Feeding Point'))
    status = models.PositiveIntegerField(
        _('Status'),
        choices=STATUS_CHOICES,
        default=PENDING
    )
    delivery_date = models.DateField(
        _('Delivery Date'),
        default=datetime.date.today
    )

    class Meta:
        unique_together = ['feeding', 'delivery_date']
        verbose_name = _('Delivery')
        verbose_name_plural = _('Deliveries')

    def __unicode__(self):
        return _('Delivery {0} for {1}'.format(self.delivery_date, self.feeding))

    def __str__(self):
        return "%s" % self.__unicode__()

    def total_weight(self):
        total_weight = 0
        delivery_products = self.deliveryproduct_set.all()
        for delivery in delivery_products:
            total_weight += delivery.quantity * delivery.product.kg_weight
        return total_weight


class Route(models.Model):
    vehicle = models.ForeignKey(Vehicle, verbose_name=_('Vehicle'))
    driver = models.ForeignKey(Driver, verbose_name=_('Driver'))
    name = models.CharField(_('Name'), max_length=255)
    observations = models.TextField(_('Observations'), blank=True)
    route_date = models.DateField(_('Date'), default=datetime.date.today)
    is_finished = models.BooleanField(_('Finished'), default=False)

    class Meta:
        verbose_name = _('Route')
        verbose_name_plural = _('Routes')

    def __unicode__(self):
        return _('Route {0} at {1}'.format(self.name, self.route_date))

    def __str__(self):
        return "%s" % self.__unicode__()


class DeliveryRoute(models.Model):
    route = models.ForeignKey(Route, verbose_name=_('Route'))
    delivery = models.ForeignKey(Delivery, verbose_name=_('Delivery'))

    class Meta:
        unique_together = ['route', 'delivery']
        verbose_name = _('Delivery Route')
        verbose_name_plural = _('Deliveries Routes')

    def __unicode__(self):
        return "{} - {}".format(self.delivery, self.route)

    def __str__(self):
        return "%s" % self.__unicode__()


class DeliveryProduct(models.Model):
    delivery = models.ForeignKey(Delivery, verbose_name=_('Delivery'))
    product = models.ForeignKey(Product, verbose_name=_('Product'))
    quantity = models.PositiveIntegerField(_('Quantity'))

    class Meta:
        verbose_name = _('Delivery Product')
        verbose_name_plural = _('Deliveries Products')

    def __unicode__(self):
        return "{} ({} {})".format(self.product, self.quantity, self.product.measurement_unit)

    def __str__(self):
        return "%s" % self.__unicode__()

    def clean(self):
        if self.quantity == 0:
            raise ValidationError(
                _('Cant create a delivery with 0 quantity'),
                code='invalid'
            )


@receiver(post_save, sender=DeliveryProduct, dispatch_uid="remove_product_stock")
def remove_product_stock(sender, instance, **kwargs):
    if instance.delivery.status == Delivery.DELIVERED:
        stock_transaction = StockTransaction(
            product=instance.product,
            quantity=instance.quantity,
            transaction_type=StockTransaction.OUTGOING
        )
        stock_transaction.save()


@receiver(post_save, sender=Route, dispatch_uid="update_delivery_status")
def update_delivery_status(sender, instance, **kwargs):
    if instance.is_finished is True:
        delivery_routes = instance.deliveryroute_set.all()
        for route in delivery_routes:
            route.delivery.status = Delivery.DELIVERED
            route.delivery.save()
            delivery_products = route.delivery.deliveryproduct_set.all()
            for delivery in delivery_products:
                stock_transaction = StockTransaction(
                    product=delivery.product,
                    quantity=delivery.quantity,
                    transaction_type=StockTransaction.OUTGOING
                )
                stock_transaction.save()
