# -*- coding: utf-8 -*-
import os
import datetime

import django.dispatch
from django.test import TestCase, Client
from django.core.urlresolvers import reverse

from django.db.models import signals

from ihungry.feeding_point.models import FeedingPoint
from ihungry.food.models import Product, StockTransaction
from .models import *


class DeliveryTest(TestCase):
    fixtures = [
        'ihungry/feeding_point/fixtures/department.json',
        'ihungry/feeding_point/fixtures/municipality.json',
        'ihungry/feeding_point/fixtures/feeding_point.json',
        'ihungry/delivery/fixtures/vehicles.json',
        'ihungry/delivery/fixtures/drivers.json',
        'ihungry/food/fixtures/product.json'
    ]

    def setUp(self):
        ''' CONF => remove_product_stock signal '''
        self.productA = Product.objects.first()
        self.deliveryA = Delivery.objects.create(
            feeding=FeedingPoint.objects.first(), status=Delivery.PENDING, delivery_date=datetime.date.today()
        )
        self.deliveryProductA = DeliveryProduct.objects.create(
            delivery=self.deliveryA, product=self.productA, quantity=25
        )
        self.deliveryA.status = Delivery.DELIVERED # Fire signal
        self.deliveryA.save()
        self.deliveryProductA.save()
        ''' CONF => update_delivery_status signal '''
        self.productB = Product.objects.last()
        self.deliveryB = Delivery.objects.create(
            feeding=FeedingPoint.objects.last(), status=Delivery.PENDING, delivery_date=datetime.date.today()
        )
        self.deliveryProductB = DeliveryProduct.objects.create(
            delivery=self.deliveryB, product=self.productB, quantity=45
        )
        self.route = Route.objects.create(
            vehicle=Vehicle.objects.first(), driver=Driver.objects.first(), name='FTHN', observations='None'
        )
        self.delivery_route = DeliveryRoute.objects.create(delivery=self.deliveryB, route=self.route)
        self.route.is_finished=True
        self.route.save()
        self.st = StockTransaction.objects.all()

    def test_signal_registry(self):
        registered_functions = [r[1]() for r in signals.post_save.receivers]
        self.assertIn(remove_product_stock, registered_functions)
        self.assertIn(update_delivery_status, registered_functions)

    def test_total_delivery_weigth(self):
        self.assertEqual(self.deliveryA.total_weight(), 870)

    def test_product_remove_units(self):
        self.assertEqual(self.productA.units, 205)

    def test_stock_transaction_exist(self):
        self.assert_(self.st.count())

    def test_number_stock_transaction(self):
        self.assertEqual(self.st.count(), 2)

    def test_stock_transaction_qty(self):
        self.assertEqual(self.st[0].quantity, 25)
        self.assertEqual(self.st[1].quantity, 45)

    def test_stock_transaction_type(self):
        self.assertNotEqual(self.st[0].transaction_type, StockTransaction.INCOMING)
        self.assertEqual(self.st[1].transaction_type, StockTransaction.OUTGOING)
