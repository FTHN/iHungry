# -*- coding: utf-8 -*-

from django.contrib import admin
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.forms import ValidationError

from ihungry.impexp import resources, fields
from ihungry.impexp.admin import ImportExportModelAdmin, ImportMixin, ExportMixin

from ihungry.feeding_point.models import FeedingPoint
from ihungry.delivery.models import Delivery, DeliveryRoute, \
    DeliveryProduct, Vehicle, Driver, Route
from ihungry.delivery.forms import DeliveryProductForm, DeliveryRouteForm


@admin.register(Vehicle)
class VehicleAdmin(ExportMixin, admin.ModelAdmin):
    list_display = ('name', 'capacity')
    search_fields = ('name', 'capacity')
    fields = (('name', 'capacity'),)


@admin.register(Driver)
class DriverAdmin(ExportMixin, admin.ModelAdmin):
    list_display = ('name', 'phone')
    search_fields = ('name', 'phone')
    fields = (('name', 'phone'),)


class DeliveryProduct(admin.StackedInline):
    model = DeliveryProduct
    extra = 1
    formset = DeliveryProductForm


class DeliveryResource(resources.ModelResource):
    status_type = fields.Field()

    class Meta:
        model = Delivery
        fields = ('feeding__name', 'status_type', 'delivery_date',)
        export_order = ('feeding__name', 'status_type', 'delivery_date')

    def dehydrate_status_type(self, obj):
        return obj.get_status_display()


@admin.register(Delivery)
class DeliveryAdmin(ExportMixin, admin.ModelAdmin):
    inlines = (DeliveryProduct, )
    resource_class = DeliveryResource
    list_display = ('feeding', 'delivery_date', 'status')
    list_filter = ('feeding', 'status', 'delivery_date')
    search_fields = ('delivery_date', 'feeding__name')
    fields = ('feeding', ('status', 'delivery_date'))

    def get_status(self, obj):
        return obj.get_status_display()

    def render_change_form(self, request, context, *args, **kwargs):
        context['adminform'].form.fields['feeding'].queryset = FeedingPoint.objects.filter(is_active=True)
        return super(DeliveryAdmin, self).render_change_form(request, context, args, kwargs)

    def add_view(self, request, form_url='', extra_context={}):
        self.inlines[0].extra = 1
        return super(DeliveryAdmin, self).add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context={}):
        self.inlines[0].extra = 0
        return super(DeliveryAdmin, self).change_view(request, object_id, form_url, extra_context)


class DeliveryRouteAdmin(admin.StackedInline):
    model = DeliveryRoute
    extra = 1
    formset = DeliveryRouteForm


@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    inlines = (DeliveryRouteAdmin, )
    list_display = ('name', 'driver', 'vehicle', 'route_date')
    list_filter = ('driver', 'vehicle', 'route_date', 'is_finished')
    search_fields = ('driver__name', 'vehicle__name', 'name')

    def add_view(self, request, form_url='', extra_context={}):
        self.inlines[0].extra = 1
        self.fields = ('vehicle', 'driver', 'name', 'observations', 'route_date')
        return super(RouteAdmin, self).add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context={}):
        self.inlines[0].extra = 0
        self.fields = ('vehicle', 'driver', 'name', 'observations', 'route_date', 'is_finished')
        return super(RouteAdmin, self).change_view(request, object_id, form_url, extra_context)
