# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext_lazy as _

from ihungry.fthn.utils import RequiredInlineFormSet


class DeliveryProductForm(RequiredInlineFormSet):

    def validate_quantity(self, obj):
        msj = _('The inventory does not have that \
            quantity of product. You only have {0} {1} of {2}.'.format(
                obj['product'].units,
                obj['product'].measurement_unit,
                obj['product']
            )
        )
        if obj['product'].units < obj['quantity']:
            raise forms.ValidationError(msj)

    def validate_product(self, objs):
        msj = _('You cant use the same product in this delivery')
        have_equals = (
            lambda products: len(products) != len(set(products))
        )( list(
                map(lambda obj: obj.id,
                    (map(lambda form: form.cleaned_data['product'], objs))
                )
            )
        )
        if have_equals:
            raise forms.ValidationError(msj)

    def clean(self):
        if self.is_valid():
            self.validate_product(self.forms)
        for form in self.forms:
            try:
                if form.cleaned_data and form.is_valid():
                    self.validate_quantity(form.cleaned_data)
            except AttributeError:
                # annoyingly, if a subform is invalid Django explicity raises
                # an AttributeError for cleaned_data
                pass


class DeliveryRouteForm(RequiredInlineFormSet):
    def clean(self):
        final_qty = 0
        for form in self.forms:
            if form.has_changed() and form.cleaned_data['delivery'].status == 4:
                raise forms.ValidationError(_('Cant use a delivery with delivered status'))
            try:
                if form.cleaned_data and form.is_valid():
                    kg_weight = form.cleaned_data['delivery'].total_weight()
                    final_qty += float(kg_weight) * 0.001 # Convert kg to tons (1 kg = 0.001 tons)
            except AttributeError:
                pass
        if self.is_valid():
            vehicle = self.instance.vehicle
            if final_qty > vehicle.capacity:
                raise forms.ValidationError(
                    _('This vehicle dont have the correct capacity for this route. \
                    %(vehicle)s only have capacity for %(capacity)s tons and your total delivery tons quantity is %(qty)s'),
                    params={
                        'vehicle': vehicle.name,
                        'capacity': vehicle.capacity,
                        'qty': final_qty
                    },
                    code='invalid'
                )
