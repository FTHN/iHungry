# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url
from django.views.generic import TemplateView


urlpatterns = [
    url(
        regex=r'^child_feeding/$',
        view=TemplateView.as_view(
            template_name='reports/child_feeding.html'
        ),
        name='child_feeding'
    ),
    url(
        regex=r'^product/$',
        view=TemplateView.as_view(
            template_name='reports/product.html'
        ),
        name='product'
    ),
    url(
        regex=r'^delivery/$',
        view=TemplateView.as_view(
            template_name='reports/delivery.html'
        ),
        name='delivery'
    ),
    url(
        regex=r'^route/$',
        view=TemplateView.as_view(
            template_name='reports/route.html'
        ),
        name='route'
    ),
]
