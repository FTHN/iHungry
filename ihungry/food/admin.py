# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from ihungry.impexp.admin import ExportMixin

from ihungry.food.models import Product, StockTransaction


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin, ExportMixin):
    list_display = ('name', 'units', 'measurement_unit')
    search_fields = ('name', 'measurement_unit')
    readonly_fields = ('units',)


@admin.register(StockTransaction)
class StockTransactionAdmin(admin.ModelAdmin, ExportMixin):
    list_display = (
        'product', 'transaction_date',
        'get_transaction_type', 'quantity',
        'prev_stock', 'final_stock'
    )
    list_filter = (
        'product', 'transaction_type', 'transaction_date'
    )
    search_fields = ('product__name',)

    def add_view(self, request, form_url='', extra_context={}):
        self.readonly_fields = ()
        self.exclude = ('transaction_date', 'prev_stock', 'final_stock')
        return super(StockTransactionAdmin, self).add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context={}):
        self.readonly_fields = (
            'product', 'quantity',
            'transaction_type', 'transaction_date',
            'prev_stock', 'final_stock'
        )
        self.exclude = ()
        return super(StockTransactionAdmin, self).change_view(request, object_id, form_url, extra_context)

    def has_delete_permission(self, request, obj=None):
        return False

    def get_transaction_type(self, obj):
        return obj.get_transaction_type_display()
    get_transaction_type.short_description = _('Transaction Type')
