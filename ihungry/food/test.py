# -*- coding: utf-8 -*-

from django.test import TestCase, Client

from .models import Product, StockTransaction

class FoodTest(TestCase):
    fixtures = ['ihungry/food/fixtures/product.json',]

    def setUp(self):
        self.prod = Product.objects.first()

    def test_product_units(self):
        self.assertEqual([prod.units for prod in Product.objects.all()], [230, 160, 150, 260, 360])

    def test_incoming(self):
        st = StockTransaction.objects.create(
            product=self.prod,
            quantity=20,
            transaction_type=StockTransaction.INCOMING
        )
        self.assertEqual(self.prod.units, 250)

    def test_outgoing(self):
        st = StockTransaction.objects.create(
            product=self.prod,
            quantity=20,
            transaction_type=StockTransaction.OUTGOING
        )
        self.assertEqual(self.prod.units, 210)
