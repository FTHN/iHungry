# -*- coding: utf-8 -*-

import datetime
from dateutil.relativedelta import relativedelta

from django.db import models
from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class Product(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    measurement_unit = models.CharField(_('Measurement Unit'), max_length=10)
    kg_weight = models.DecimalField(
        _('Weight'), max_digits=3,
        decimal_places=1, null=True,
        help_text=_('Weight in kilograms')
    )
    units = models.PositiveIntegerField(_('Unit'), default=0)

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return "%s" % self.__unicode__()

    def total_transaction(self, transaction, months=1):
        current_date = datetime.date.today()
        past_date = current_date - relativedelta(months=months, day=1)
        amount = self.stocktransaction_set.filter(
            transaction_type=transaction,
            transaction_date__gte=past_date,
            transaction_date__lte=current_date
        ).aggregate(q=Sum('quantity'))
        return amount['q']


class StockTransaction(models.Model):
    INCOMING, OUTGOING = range(1, 3)
    CATEGORY_CHOICES = (
        (INCOMING, _('Incoming')),
        (OUTGOING, _('Outgoing'))
    )

    product = models.ForeignKey(Product, verbose_name=_('Product'))
    quantity = models.PositiveIntegerField(_('Quantity'))
    transaction_type = models.PositiveIntegerField(
        _('Transaction Type'),
        choices=CATEGORY_CHOICES,
        default=INCOMING
    )
    transaction_date = models.DateField(
        _('Transaction Date'), default=datetime.date.today
    )
    prev_stock = models.PositiveIntegerField(_('Previous Value'), default=0)
    final_stock = models.PositiveIntegerField(_('Final Value'), default=0)

    class Meta:
        verbose_name = _('Stock Transaction')
        ordering = ('-transaction_date',)
        verbose_name_plural = _('Stock Transactions')

    def __unicode__(self):
        return "{} - {} - {}".format(
            self.product, self.quantity,
            self.get_transaction_type_display()
        )

    def __str__(self):
        return "%s" % self.__unicode__()

    def clean(self):
        if self.quantity == 0:
            raise ValidationError(
                _('Cant create a stock transaction with 0 quantity'),
                code='invalid'
            )

    def save(self, *args, **kwargs):
        if not self.id:
            self.prev_stock = self.product.units
            if self.transaction_type is self.INCOMING:
                self.product.units += self.quantity
            elif self.transaction_type is self.OUTGOING:
                self.product.units -= self.quantity
            self.final_stock = self.product.units
            self.product.save()
        super(StockTransaction, self).save(*args, **kwargs)
