# -*- coding: utf-8 -*-

from django.conf.urls import url

from rest_framework.routers import SimpleRouter

from ihungry.api.views import DepartmentViewSet, ChildrenViewSet, \
    TransactionViewSet, FeedPointViewSet, ChildrenViewSet2, \
    ReportChildFeeding, ProductStock, StockHistory, ReportProduct, \
    ReportDelivery, ReportRoute

router = SimpleRouter()
router.register(r'department', DepartmentViewSet, base_name='department-view')
router.register(r'children', ChildrenViewSet, base_name='children-view')
router.register(r'transaction', TransactionViewSet, base_name='transaction-view')
router.register(r'feeding-point', FeedPointViewSet, base_name='feedingpoint-view')
router.register(r'children2', ChildrenViewSet2, base_name='children2-view')

urlpatterns = [
    url(r'^product-stock/$', ProductStock, name="product-stock"),
    url(r'^stock-history/$', StockHistory, name="stock-history"),
    url(r'^report-product/$', ReportProduct, name="report-product"),
    url(r'^report-child-feeding/$', ReportChildFeeding, name="report-child-feeding"),
    url(r'^report-delivery/$', ReportDelivery, name="report-delivery"),
    url(r'^report-route/$', ReportRoute, name="report-route"),
]

urlpatterns += router.urls
