# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase, Client

from ihungry.api.urls import urlpatterns


client = Client()


class TestAllView(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            'test_user', 'user@test.net', 'secret'
        )
        self.user.save()
        client.login(username=self.user.username, password='secret')
        self.url_names = ['api_v1:' + p.name for p in urlpatterns]

    def test_views_status(self):
        for url_name in self.url_names:
            try:
                response = client.get(reverse(url_name))
            except:
                try:
                    response = client.get(reverse(url_name, args=[1]))
                except:
                    response = client.get(reverse(url_name, args=[1, 1]))
            self.assertEqual(response.status_code, 200)
