# -*- coding: utf-8 -*-

import datetime
import operator

from dateutil.relativedelta import relativedelta
from operator import itemgetter
from itertools import groupby
from functools import reduce

from django.db.models import Count
from django.db.models import Q
from django.db.models.functions import TruncMonth
from django.utils.translation import ugettext_lazy as _

import django_filters.rest_framework

from rest_framework import viewsets
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view

from django_filters.rest_framework import DjangoFilterBackend

from ihungry.food.models import Product, StockTransaction
from ihungry.delivery.models import Delivery, Route
from ihungry.feeding_point.models import Department, \
    Municipality, FeedingPoint, Children

from ihungry.api.serializers import DepartmentSerializer, \
    ChildrenSerializer, TransactionSerializer, FeedPointSerializer, \
    ChildrenSerializer2


@api_view(['GET'])
def ReportChildFeeding(request):
    # XXX: ONLY EDIT THIS FUNCTION IF
    # God grant you the serenity to accept the things that you cannot change,
    # the courage to change the things that you can,
    # and the wisdom to know the difference.
    # XXX: TRUST ME. I'M GOD

    resp, query, params = list(), dict(), request.GET

    if 'feedings' in params:
        if len(params.get('feedings')):
            query['feeding__in'] = [int(x) for x in params.get('feedings').split(',')]

        childs = Children.objects.filter(**query)
        headers = [
            {'header': _('Feeding Point'), 'accessor': 'feeding'},
            {'header': _('First Name'), 'accessor': 'firstName'},
            {'header': _('Last Name'), 'accessor': 'lastName'},
            {'header': _('Age'), 'accessor': 'age'}
        ]
        childs_records = []
        grouper = itemgetter('feeding_pk')
        for child in childs:
            childs_records.append({
                'feeding_pk': child.feeding.pk,
                'is_active': child.is_active,
                'feeding': child.feeding.name,
                'firstName': child.first_name,
                'lastName': child.last_name,
                'age': child.children_age()
            })
        childs_grouped = groupby(sorted(childs_records, key=grouper), key=grouper)
        for feeding, childs in childs_grouped:
            feed = FeedingPoint.objects.get(pk=feeding)
            feeding = {
                'name': feed.name, 'dir': feed.address,
                'dept': feed.department.name, 'mun': feed.municipality.name,
                'childs': feed.children_set.all().count()
            }
            childs_records = []
            for child in childs:
                childs_records.append(child)
            resp.append(
                dict(
                    [('columns', headers), ('records', childs_records),
                    ('chart', feed.active_childrens()), ('feeding', feeding)]
                )
            )

        return Response(resp)

    else:
        for fp in FeedingPoint.objects.all():
            resp.append({
                'value': fp.pk,
                'label': fp.name
            })
        return Response(resp)

    return Response()


@api_view(['GET'])
def ReportProduct(request):
    resp, params = [], request.GET
    # date Format = YYYY-MM-DD
    if len(params.dict()):

        if len(params.get('products')):
            products = [int(x) for x in params.get('products').split(',')]
        else:
            products = []
        for idx, prd in enumerate(products):
            sti, sto = list(), list()
            prod = Product.objects.get(pk=prd)
            resp.append({
                'product': {'name': prod.name, 'um': prod.measurement_unit, 'kg': prod.kg_weight, 'units': prod.units}
            })

            qs = StockTransaction.objects.filter(
                transaction_date__gte=params.get('startDate'),
                transaction_date__lte=params.get('endDate'),
                product=prod
            )
            for obj in qs.filter(transaction_type=StockTransaction.INCOMING):
                sti.append({
                    'qty': obj.quantity,
                    'date': obj.transaction_date,
                    'type': obj.get_transaction_type_display()
                })
            resp[idx]['sti'] = sti
            for obj in qs.filter(transaction_type=StockTransaction.OUTGOING):
                sto.append({
                    'qty': obj.quantity,
                    'date': obj.transaction_date,
                    'type': obj.get_transaction_type_display()
                })
            resp[idx]['sto'] = sto
    else:
        for prod in Product.objects.all():
            resp.append({
                'value': prod.pk,
                'label': prod.name
            })
    return Response(resp)


@api_view(['GET'])
def ReportDelivery(request):
    resp, query, params = [], {}, request.GET
    if len(params.dict()):
        if len(params.get('feeding')):
            feedings = [int(x) for x in params.get('feeding').split(',')]
        else:
            feedings = []
        if len(params.get('state')):
            states = [int(x) for x in params.get('state').split(',')]
            qlist = []
            for state in states:
                qlist.append(Q(status=state))
        for idx, feed in enumerate(feedings):
            delivery = list()
            feeding = FeedingPoint.objects.get(pk=feed)
            resp.append({
                'feeding': {
                    'name': feeding.name, 'dir': feeding.address,
                    'mun': feeding.municipality.name, 'dept': feeding.department.name,
                    'childs': feeding.children_set.all().count(), 'chart': feeding.active_childrens()
                }
            })
            query['feeding'] = feeding
            query['delivery_date__gte'] = params.get('startDate')
            query['delivery_date__lte'] = params.get('endDate')
            deliveries = Delivery.objects.filter(reduce(operator.or_, qlist), **query)
            for it, obj in enumerate(deliveries):
                delivery_data = {
                    'feeding': obj.feeding.name,
                    'date': obj.delivery_date,
                    'status': obj.get_status_display(),
                    'products': [],
                }
                for prod in obj.deliveryproduct_set.all():
                    delivery_data['products'].append({
                        'name': prod.product.name,
                        'qty': prod.quantity,
                        'mu': prod.product.measurement_unit
                    })
                delivery.append(delivery_data)
            resp[idx]['deliveries'] = delivery
    else:
        for fp in FeedingPoint.objects.all():
            resp.append({
                'value': fp.pk,
                'label': fp.name
            })
    return Response(resp)


@api_view(['GET'])
def ReportRoute(request):
    resp, params = list(), request.GET
    if 'routes' in params:
        if len(params.get('routes')):
            routes = [int(x) for x in params.get('routes').split(',')]
            for idx, route in enumerate(Route.objects.filter(pk__in=routes)):
                deliveries = route.deliveryroute_set.all()
                resp.append({
                        'name': route.name, 'driver': route.driver.name,
                        'vehicle': route.vehicle.name, 'date': route.route_date,
                        'delivery_total': deliveries.count(), 'chart': [],
                        'notes': route.observations, 'deliveries': []
                })
                feedings = deliveries.values('delivery__feeding').annotate(dcount=Count('delivery__feeding'))
                resp[idx]['chart'] = [
                    {
                        'name': FeedingPoint.objects.get(pk=feeding['delivery__feeding']).name,
                        'value': feeding['dcount']
                    } for feeding in feedings
                ]
                for index, delivery in enumerate(deliveries):
                    resp[idx]['deliveries'].append({
                        'feeding': delivery.delivery.feeding.name, 'date': delivery.delivery.delivery_date,
                        'status': delivery.delivery.get_status_display(), 'products': []
                    })
                    for product in delivery.delivery.deliveryproduct_set.all():
                        resp[idx]['deliveries'][index]['products'].append({
                            'name': product.product.name, 'qty': product.quantity
                        })

    else:
        for route in Route.objects.all():
            resp.append({
                'value': route.pk,
                'label': route.name
            })
    return Response(resp)


@api_view(['GET'])
def ProductStock(request):
    resp = list()
    for prod in Product.objects.all():
        resp.append({
            'name': prod.name,
            'inp': prod.total_transaction(StockTransaction.INCOMING),
            'out': prod.total_transaction(StockTransaction.OUTGOING),
            'amt': prod.units
        })
    return Response(resp)


@api_view(['GET'])
def StockHistory(request):
    current_date = datetime.date.today()
    past_six = current_date - relativedelta(months=6)
    records = Delivery.objects.filter(
        status=Delivery.DELIVERED,
        delivery_date__gte=past_six,
        delivery_date__lte=current_date
    ).annotate(
        month=TruncMonth('delivery_date')
    ).values('month').annotate(qty=Count('id'))
    return Response(records)


class DepartmentViewSet(viewsets.ViewSet):
    """
    API endpoint that show departments and number of feeding_points
    """
    def list(self, request):
        queryset = Department.objects.annotate(nfp=Count('feedingpoint'))[:5]
        serializer = DepartmentSerializer(queryset, many=True)
        return Response(serializer.data)


class ChildrenViewSet(viewsets.ViewSet):
    """
    API endpoint that show departments with max number of childrens
    """
    def list(self, request):
        queryset = Department.objects.annotate(nc=Count('feedingpoint__children'))[:5]
        serializer = ChildrenSerializer(queryset, many=True)
        return Response(serializer.data)


class TransactionViewSet(viewsets.ViewSet):
    """ API endpoint for Stock Transaction """
    def list(self, request):
        headers = [_('Product'), _('Transaction Type'), _('Date')]
        queryset = StockTransaction.objects.all().order_by('-transaction_date')[:5]
        serializer = TransactionSerializer(queryset, many=True)
        data = {'headers': headers, 'records': serializer.data}
        return Response(data)


class FeedPointViewSet(viewsets.ViewSet):
    """ API endpoint for Stock Transaction """
    def list(self, request):
        headers = [_('Name'), _('Place'), _('Date')]
        queryset = FeedingPoint.objects.all().order_by('-created')[:5]
        serializer = FeedPointSerializer(queryset, many=True)
        data = {'headers': headers, 'records': serializer.data}
        return Response(data)


class ChildrenViewSet2(viewsets.ViewSet):
    """ API endpoint for Stock Transaction """
    def list(self, request):
        headers = [_('Name'), _('Age'), _('Feeding Point')]
        queryset = Children.objects.all()[:5]
        serializer = ChildrenSerializer2(queryset, many=True)
        data = {'headers': headers, 'records': serializer.data}
        return Response(data)
