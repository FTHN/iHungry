# -*- coding: utf-8 -*-

from rest_framework import serializers

from ihungry.feeding_point.models import Department, FeedingPoint, Children
from ihungry.food.models import StockTransaction


class DepartmentSerializer(serializers.ModelSerializer):
    number_feeding_points = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Department
        fields = ('name', 'iso_code', 'number_feeding_points')

    def get_number_feeding_points(self, obj):
        return obj.nfp


class ChildrenSerializer(serializers.ModelSerializer):
    number_childs = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Department
        fields = ('name', 'iso_code', 'number_childs')

    def get_number_childs(self, obj):
        return obj.nc


class TransactionSerializer(serializers.ModelSerializer):
    product_name = serializers.SerializerMethodField()
    t_type = serializers.SerializerMethodField()

    class Meta:
        model = StockTransaction
        fields = ('product_name', 't_type','transaction_date')

    def get_product_name(self, obj):
        return obj.product.name

    def get_t_type(self, obj):
        return obj.get_transaction_type_display()


class FeedPointSerializer(serializers.ModelSerializer):
    place = serializers.SerializerMethodField()
    create_date = serializers.SerializerMethodField()

    class Meta:
        model = FeedingPoint
        fields = ('name', 'place', 'create_date')

    def get_place(self, obj):
        return "{} - {}".format(obj.department.name, obj.municipality.name)

    def get_create_date(self, obj):
        return obj.created.date()


class ChildrenSerializer2(serializers.ModelSerializer):
    feeding_name = serializers.SerializerMethodField()

    class Meta:
        model = Children
        fields = ('get_full_name', 'children_age', 'feeding_name')

    def get_feeding_name(self, obj):
        return obj.feeding.name
