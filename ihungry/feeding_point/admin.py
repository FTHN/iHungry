# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from nested_admin import NestedStackedInline, \
    NestedModelAdmin

from ihungry.impexp.admin import ExportMixin, ImportExportModelAdmin

from ihungry.fthn.utils import RequiredInlineFormSet

from ihungry.feeding_point.models import FeedingPoint, \
    FeedingResponsable, Department, Municipality, \
    FeedingPointSettings, Phone, Children, ChildrenDetail


class MunicipalityInline(admin.StackedInline):
    model = Municipality
    extra = 1


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    inlines = (MunicipalityInline, )
    list_display = ('name', 'iso_code')
    search_fields = ('name', 'municipality__name')


class PhoneInline(NestedStackedInline):
    model = Phone
    extra = 0
    is_sortable = False
    fields = (('phone', 'operator'),)


class FeedingResponsableAdmin(NestedStackedInline):
    inlines = (PhoneInline, )
    model = FeedingResponsable
    extra = 1
    formset = RequiredInlineFormSet
    is_sortable = False
    fields = (
        ('name', 'email'),
        ('role', 'communication_level')
    )


@admin.register(FeedingPoint)
class FeedingPointAdmin(ExportMixin, NestedModelAdmin):
    inlines = (FeedingResponsableAdmin,)
    # TODO: Check admin filters
    list_filter = ('is_ong', 'is_active', 'created')
    list_display = ('name', 'department', 'municipality', 'created')
    search_fields = ('department__name', 'municipality__name', 'name')
    fields = (
        'name', ('department', 'municipality'),
        ('dinning_rooms', 'is_ong', 'is_active'),
        ('address', 'geoposition')
    )


    def add_view(self, request, form_url='', extra_context={}):
        self.inlines[0].extra = 1
        return super(FeedingPointAdmin, self).add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context={}):
        self.inlines[0].extra = 0
        return super(FeedingPointAdmin, self).change_view(request, object_id, form_url, extra_context)


class ChildrenInline(admin.StackedInline):
    model = ChildrenDetail
    extra = 1


@admin.register(Children)
class ChildrenAdmin(ImportExportModelAdmin):
    inlines = (ChildrenInline, )
    list_filter = ('feeding', )
    list_display = (
        'first_name', 'last_name', 'children_age', 'feeding',
        'feeding_department', 'feeding_municipality'
    )
    search_fields = ('first_name', 'last_name', 'feeding__name')

    def feeding_municipality(self, obj):
        return obj.feeding.municipality
    feeding_municipality.short_description = _('Municipality')

    def feeding_department(self, obj):
        return obj.feeding.department
    feeding_department.short_description = _('Department')


@admin.register(FeedingPointSettings)
class FPSettingsAdmin(admin.ModelAdmin):
    list_display = ('feeding', 'year', 'frequency_month', 'week', 'day')
    list_filter = ('feeding', 'year', 'frequency_month', 'week', 'day')
    fields = ('feeding', 'year', 'frequency_month', 'day', 'week')

    def add_view(self, request, form_url='', extra_context={}):
        self.readonly_fields = ()
        return super(FPSettingsAdmin, self).add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context={}):
        self.readonly_fields = ('feeding', 'year', 'frequency_month', 'day', 'week')
        return super(FPSettingsAdmin, self).change_view(request, object_id, form_url, extra_context)

    def has_delete_permission(self, request, obj=None):
        return False

    def frequency_month(self, obj):
        return obj.get_frequency_month_display()
    frequency_month.short_description = _('Frecuency Month')
