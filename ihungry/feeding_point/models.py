# -*- coding: utf-8 -*-
import datetime
import calendar

from dateutil.relativedelta import relativedelta

from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.core.exceptions import ValidationError

from smart_selects.db_fields import ChainedForeignKey

from .fields import PlacesField


class Department(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    iso_code = models.CharField(_('Code'), max_length=20)

    class Meta:
        verbose_name = _('Department')
        verbose_name_plural = _('Departments')
        ordering = ['pk', 'name']

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()


class Municipality(models.Model):
    department = models.ForeignKey(Department, verbose_name=_('Department'))
    name = models.CharField(_('Name'), max_length=255)

    class Meta:
        verbose_name = _('Municipality')
        verbose_name_plural = _('Municipalities')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()


class FeedingPoint(models.Model):
    department = models.ForeignKey(Department, verbose_name=_('Department'))
    municipality = ChainedForeignKey(
        Municipality,
        chained_field="department",
        chained_model_field="department",
        show_all=False,
        auto_choose=True,
        sort=True,
        verbose_name=_('Municipality')
    )

    name = models.CharField(_('Name'), max_length=255)
    dinning_rooms = models.PositiveIntegerField(_('Dinning Rooms'), default=1)
    geoposition = PlacesField(_('Geoposition'), blank=True)
    address = models.TextField(_('Address'))
    is_ong = models.BooleanField(_('Is ONG'), default=False)
    is_active = models.BooleanField(_('Is Active'), default=True)
    created = models.DateTimeField(_('Created'), auto_now_add=True)

    class Meta:
        verbose_name = _('Feeding Point')
        verbose_name_plural = _('Feeding Points')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return "%s" % self.__unicode__()

    @property
    def coordinates(self):
        return self.geoposition.split(',')

    def active_childrens(self):
        childs = self.children_set.all()
        act, ina = childs.filter(is_active=True).count(), childs.filter(is_active=False).count()
        return {'name': _('Active'), 'value': act}, {'name': _('Inactive'), 'value': ina}


class FeedingPointSettings(models.Model):
    MO, TU, WE, TH, FR, SA, SU = range(0,7)
    WEEK_DAY_CHOICES = (
        (MO, _("Monday")),
        (TU, _("Tuesday")),
        (WE, _("Wednesday")),
        (TH, _("Thursday")),
        (FR, _("Friday")),
        (SA, _("Saturday")),
        (SU, _("Sunday"))
    )
    FW, SW, TW, QW = range(0,4)
    MONTH_WEEK_CHOICES = (
        (FW, _('First Week')),
        (SW, _('Second Week')),
        (TW, _('Third Week')),
        (QW, _('Quarter Week'))
    )
    FREQUENCY_MONTH_CHOICES = (
        (1, _('Monthly')),
        (2, _('Bimester')),
        (3, _('Trimester')),
        (4, _('Quarter')),
        (5, _('Quinquemestre')),
        (6, _('Semester'))
    )
    feeding = models.ForeignKey(FeedingPoint, verbose_name=_('Feeding'))
    year = models.PositiveIntegerField(
        _('Year'),
        choices=list(map(lambda y: (y, y), range(2017, 2027))),
        default=datetime.datetime.now().year
    )
    frequency_month = models.PositiveIntegerField(
        _('Frequency Month'), choices=FREQUENCY_MONTH_CHOICES,
        default=1, help_text=_('Number of months to go in the year')
    )
    day = models.PositiveIntegerField(_('Day'), choices=WEEK_DAY_CHOICES, default=MO)
    week = models.PositiveIntegerField(_('Week'), choices=MONTH_WEEK_CHOICES, default=FW)
    start_date = models.DateField(_('Start Date'))

    def save(self, *args, **kwargs):
        ''' On save, update start_date '''
        if not self.id and not self.start_date:
            self.start_date = datetime.date.today()
        return super(FeedingPointSettings, self).save(*args, **kwargs)

    def clean(self):
        next_month = datetime.date.today() + relativedelta(months=1)
        if next_month.year > self.year:
            raise ValidationError(
                _('Cant create the Delivery in the last month of the year'),
                code='invalid'
            )

    class Meta:
        unique_together = ['feeding', 'year']
        verbose_name = _('Feeding Point Setting')
        verbose_name_plural = _('Feeding Point Settings')

    def __unicode__(self):
        return self.feeding.name

    def __str__(self):
        return "%s" % self.__unicode__()


class FeedingResponsable(models.Model):
    INITIAL, GOOD, VERY_GOOD, EXCELENT = range(1,5)
    COMMUNICATION_LEVEL_CHOICES = (
        (INITIAL, _('Initial')),
        (GOOD, _('Good')),
        (VERY_GOOD, _('Very Good')),
        (EXCELENT, _('Excelent')),
    )

    feeding = models.ForeignKey(
        FeedingPoint, null=True,
        verbose_name=_('Feeding Responsable')
    )
    name = models.CharField(_('Name'), max_length=255)
    role = models.CharField(_('Role'), max_length=255, null=True)
    email = models.EmailField(_('Email'))
    communication_level = models.PositiveIntegerField(
        _('Communication Level'),
        choices=COMMUNICATION_LEVEL_CHOICES,
        default=INITIAL
    )

    class Meta:
        verbose_name = _('Feeding Responsable')
        verbose_name_plural = _('Feeding Responsables')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return "%s" % self.__unicode__()


# TODO: CHANGE THIS MODEL NAME -_-""
class Phone(models.Model):
    MOVISTAR, CLARO = range(1,3)
    OPERATOR_CHOICES = (
        (MOVISTAR, 'Movistar'),
        (CLARO, 'Claro')
    )

    responsable = models.ForeignKey(FeedingResponsable)
    phone = models.CharField(_('Phone'), max_length=20)
    operator = models.PositiveIntegerField(
        _('Mobile Operator'),
        choices=OPERATOR_CHOICES,
        default=CLARO
    )

    class Meta:
        verbose_name = _('Phone')
        verbose_name_plural = _('Phones')

    def __unicode__(self):
        return "{} - {} - {}".format(self.responsable, self.phone, self.get_operator_display())

    def __str__(self):
        return "%s" % self.__unicode__()


class Children(models.Model):
    feeding = models.ForeignKey(FeedingPoint, verbose_name=_('Feeding Point'))
    first_name = models.CharField(_('First Name'), max_length=255)
    last_name = models.CharField(_('Last Name'), max_length=255)
    is_active = models.BooleanField(_('Is Active'), default=True)

    class Meta:
        verbose_name = _('Children')
        verbose_name_plural = _('Childrens')

    @property
    def get_full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    def children_age(self):
        children = self.childrendetail_set.last()
        return children.age if children is not None else None
    children_age.short_description = _('Age')
    children_age.empty_value_display = '---'

    def __unicode__(self):
        return self.get_full_name

    def __str__(self):
        return "%s" % self.__unicode__()


class ChildrenDetail(models.Model):
    ''' Store Children Data related by year'''
    children = models.ForeignKey(Children, verbose_name=_('Children'))
    grade = models.CharField(_('Grade'), max_length=255, null=True)
    age = models.PositiveIntegerField(_('Age'), default=1)
    weight = models.CharField(_('Weight'), max_length=255, null=True)
    height = models.CharField(_('Height'), max_length=255, null=True)
    created = models.DateField(_('Created'), default=datetime.date.today)

    class Meta:
        verbose_name = _('Children Detail')
        verbose_name_plural = _('Childrens Details')

    def __unicode__(self):
        return _('Detail for {0} - {1}'.format(self.children, self.created))

    def __str__(self):
        return "%s" % self.__unicode__()


@receiver(post_save, sender=FeedingPointSettings, dispatch_uid="create_delivery_by_feeding_settings")
def create_delivery_by_feeding_settings(sender, instance, **kwargs):
    '''
    You may think you know what the following code does.
    But you dont. Trust me.
    Fiddle with it, and you'll spend many a sleepless
    night cursing the moment you thought youd be clever
    enough to "optimize" the code below.
    Now close this file and go play with something else.
    '''
    from ihungry.delivery.models import Delivery

    c = calendar.Calendar(firstweekday=calendar.MONDAY) # init calendar

    '''Create the first feeding delivery its always be the next month'''

    start_date = instance.start_date + relativedelta(months=1) # next month
    number_of_delivery = int((12 - start_date.month) / instance.frequency_month) # Calculate the number of delivery

    if start_date.year == instance.year:
        year = start_date.year; month = start_date.month # configure calendar
        monthcal = c.monthdatescalendar(year, month) # get calendar based on year and month
        # Get the specific day in the specific week of the month
        selected_date = [day for week in monthcal for day in week if \
            day.weekday() == instance.day and day.month == month][instance.week]
        # Create the first delivery
        Delivery.objects.create(
            feeding=instance.feeding, status=Delivery.PENDING,
            delivery_date=selected_date
        )
        ''' Create the rest of delivery in the year '''
        delivery_date = start_date + relativedelta(months=instance.frequency_month) # Init with the next delivery
        for foo in range(number_of_delivery):
            monthcal = c.monthdatescalendar(year, delivery_date.month) # get a new month calendar
            selected_date = [day for week in monthcal for day in week if \
                day.weekday() == instance.day and day.month == delivery_date.month][instance.week]
            # Create the delivery
            Delivery.objects.create(
                feeding=instance.feeding, status=Delivery.PENDING,
                delivery_date=selected_date
            )
            # Update the delivery date based  previous date + frecuency_month
            delivery_date += relativedelta(months=instance.frequency_month)
