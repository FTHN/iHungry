# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import functools
import operator

from django import forms
from django.utils.translation import ugettext_lazy as _

from .widgets import PlacesWidget, ToggleCheckboxes
from . import Places


class PlacesField(forms.MultiValueField):
    default_error_messages = {
        'invalid': _('Enter a valid geoposition.')
    }

    def __init__(self, *args, **kwargs):
        self.widget = PlacesWidget()
        fields = (
            forms.DecimalField(label=_('latitude')),
            forms.DecimalField(label=_('longitude')),
        )
        if 'initial' in kwargs:
            kwargs['initial'] = Location(*kwargs['initial'].split(','))
        super(PlacesField, self).__init__(fields, **kwargs)

    def widget_attrs(self, widget):
        classes = widget.attrs.get('class', '').split()
        classes.append('places')
        return {'class': ' '.join(classes)}

    def compress(self, value_list):
        if value_list:
            return value_list
        return ""
