# -*- coding: utf-8 -*-
import datetime

import django.dispatch
from django.test import TestCase, Client
from django.core.urlresolvers import reverse

from django.db.models import signals

from ihungry.delivery.models import Delivery

from .models import *


class FeedingPointTest(TestCase):
    fixtures = [
        'ihungry/feeding_point/fixtures/department.json',
        'ihungry/feeding_point/fixtures/municipality.json',
        'ihungry/feeding_point/fixtures/feeding_point.json',
        'ihungry/feeding_point/fixtures/childrens.json',
        'ihungry/feeding_point/fixtures/feeding_responsable.json',
    ]

    def setUp(self):
        self.feeding_point = FeedingPoint.objects.last()
        self.children =  Children.objects.first()
        self.fps = FeedingPointSettings.objects.create(
            feeding=self.feeding_point, year=2017,
            frequency_month=2, day=3, week=3,
            start_date=datetime.datetime(2017, 4, 12, 00).date()
        )

    def test_signal_registry(self):
        registered_functions = [r[1]() for r in signals.post_save.receivers]
        self.assertIn(create_delivery_by_feeding_settings, registered_functions)

    def test_feeding_responsable(self):
        self.assertNotIn(FeedingResponsable.objects.first(), self.feeding_point.feedingresponsable_set.all())
        self.assertIn(FeedingResponsable.objects.get(pk=2), self.feeding_point.feedingresponsable_set.all())

    def test_first_delivery(self):
        self.assert_(Delivery.objects.get(feeding=self.feeding_point, delivery_date=datetime.datetime(2017, 5, 25, 00).date()))

    def test_number_delivery(self):
        self.assertEqual(Delivery.objects.all().count(), 4)

    def test_delivery_dates(self):
        self.assertEqual([str(d.delivery_date) for d in Delivery.objects.all()], ['2017-05-25', '2017-07-27', '2017-09-28', '2017-11-23'])
