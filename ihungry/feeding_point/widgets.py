# -*- coding: utf-8 -*-

from django import forms
from django.forms.widgets import CheckboxSelectMultiple
from django.utils import six
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

from .conf import settings


class PlacesWidget(forms.MultiWidget):

    def __init__(self, attrs=None):
        widgets = (
            forms.TextInput(attrs={'data-geo': 'lat',}),
            forms.TextInput(attrs={'data-geo': 'lng',}),
        )
        super(PlacesWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if isinstance(value, six.text_type):
            return value.rsplit(',')
        if value:
            return [value.latitude, value.longitude]
        return [None, None]

    def format_output(self, rendered_widgets):
        return render_to_string('admin/_map_widget.html', {
            'latitude': {
                'html': rendered_widgets[0],
                'label': _("latitude"),
            },
            'longitude': {
                'html': rendered_widgets[1],
                'label': _("longitude"),
            },
            'config': {
                'key': settings.MAPS_API_KEY,
            }
        })


class ToggleCheckboxes(CheckboxSelectMultiple):
    class Media:
        js = (
            'weekday_field/js/checkbox-widget.js',
        )

    def render(self, name, value, attrs=None):
        if attrs is None:
            attrs = {}
        if utils.is_str(value):
            value = [int(i) for i in value.split(',') if i]
        if not value or value == range(7):
            value = [str(x[0]) for x in self.choices]
        if all([x in value for x in range(5)]):
            value.append('0,1,2,3,4')
        if 5 in value and 6 in value:
            value.append('5,6')
        if 'class' not in attrs:
            attrs['class'] = ""
        attrs['class'] += " advanced-weekday-field"
        result = super(ToggleCheckboxes, self).render(name, value, attrs)
        result = result.replace(
            " Weekends</label></li>",
            " Weekends</label></li></ul><ul>")
        return mark_safe(result)

    def value_from_datadict(self, data, files, name):
        value = super(ToggleCheckboxes, self).value_from_datadict(data, files, name)
        return sorted(set([x for sub in value for x in sub.split(',')]))
