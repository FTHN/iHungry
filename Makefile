run:
	python manage.py runserver

deploy:
	git push heroku master
	heroku run python manage.py migrate
	heroku run python manage.py check --deploy
	heroku open

dump_local:
	python manage.py loaddata ihungry/delivery/fixtures/drivers.json
	python manage.py loaddata ihungry/delivery/fixtures/vehicles.json
	python manage.py loaddata ihungry/feeding_point/fixtures/department.json
	python manage.py loaddata ihungry/feeding_point/fixtures/municipality.json
	python manage.py loaddata ihungry/feeding_point/fixtures/feeding_point.json
	python manage.py loaddata ihungry/feeding_point/fixtures/feeding_responsable.json
	python manage.py loaddata ihungry/feeding_point/fixtures/childrens.json
	python manage.py loaddata ihungry/food/fixtures/product.json
	python manage.py loaddata ihungry/food/fixtures/stock_transaction.json
	python manage.py loaddata ihungry/delivery/fixtures/delivery.json
	python manage.py loaddata ihungry/delivery/fixtures/delivery_product.json

dump_heroku:
	heroku run python manage.py loaddata ihungry/delivery/fixtures/drivers.json
	heroku run python manage.py loaddata ihungry/delivery/fixtures/vehicles.json
	heroku run python manage.py loaddata ihungry/feeding_point/fixtures/department.json
	heroku run python manage.py loaddata ihungry/feeding_point/fixtures/municipality.json
	heroku run python manage.py loaddata ihungry/feeding_point/fixtures/feeding_point.json
	heroku run python manage.py loaddata ihungry/feeding_point/fixtures/feeding_responsable.json
	heroku run python manage.py loaddata ihungry/feeding_point/fixtures/childrens.json
	heroku run python manage.py loaddata ihungry/food/fixtures/product.json
	heroku run python manage.py loaddata ihungry/food/fixtures/stock_transaction.json
	heroku run python manage.py loaddata ihungry/delivery/fixtures/delivery.json
	heroku run python manage.py loaddata ihungry/delivery/fixtures/delivery_product.json

setup_heroku:
	heroku pg:reset DATABASE_URL --confirm quiet-lowlands-62704
	make deploy
	heroku run python manage.py createsuperuser

build_react:
	rm -rf ihungry/static/components/bundles
	BABEL_ENV=production ./node_modules/.bin/webpack --config webpack.config.js

watch:
	python watch.py

messages:
	django-admin makemessages -l es

compile:
	django-admin compilemessages -l es
